import { ApiBaseResponse } from "../api/response.js";
import userRepository from "../repository/user.js";
import { DeleteUserRequest, User } from "../model/user.js";

const getUsers = async (req, res) => {
  try {
    // throw
    const users = await userRepository.findUsers();
    const response = new ApiBaseResponse({
      data: users,
      message: "Success",
    });
    return res.send(response);
  } catch (error) {
    const response = new ApiBaseResponse({
      data: null,
      message: "Failed",
      error: error.message,
    });
    return res.send(response);
  }
};

const register = async (req, res) => {
  try {
    const user = new User(req.body);
    //save to db
    user.validate();
    await userRepository.save(user);
    const response = new ApiBaseResponse({
      message: "register success",
      data: user,
    });
    return res.send(response);
  } catch (error) {
    const response = new ApiBaseResponse({
      data: null,
      message: "Failed",
      error: error.message,
    });
    return res.send(response);
  }
};

const deleteUser = async (req, res) => {
  try {
    const { username } = req.params;
    const deleteReq = new DeleteUserRequest({ username });
    deleteReq.validate();
    const affectedRows = await userRepository.deleteUserByUsername(deleteReq);
    if (affectedRows === 0) {
      throw new Error("User not found");
    }
    const response = new ApiBaseResponse({
      message: "delete success",
      data: username,
    });
    return res.send(response);
  } catch (error) {
    const response = new ApiBaseResponse({
      data: null,
      message: "delete user failed",
      error: error.message,
    });
    return res.send(response);
  }
};

export default {
  getUsers,
  register,
  deleteUser,
};

// export class UserController {
//   repository;

//   constructor(repository) {
//     this.repository = repository;
//     this.register = this.register.bind(this);
//   }

//   async register(req, res) {
//     const user = new User(req.body);
//     console.log(user);
//     // console.log(this.repository)
//     //save to
//     try {
//       const result = await this.repository.save(user);
//       console.log(result);
//       //response to client
//       res.send("success");
//     } catch (error) {
//       console.log(error);
//       res.status(500).send("error");
//     }
//   }
// }
