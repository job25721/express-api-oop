export class ApiBaseResponse {
  message;
  data;
  error;

  constructor({ message = "", data = null, error = null }) {
    this.message = message;
    this.data = data;
    this.error = error;
  }
}
