import {
  DELETE_USER_REQUEST_ERROR,
  REGISTER_REQUEST_ERROR,
} from "../error_msg/user.js";

export class User {
  username;
  password;

  constructor({ username, password }) {
    this.username = username;
    this.password = password;
  }

  validate() {
    if (!this.username || !this.password) {
      throw new Error(REGISTER_REQUEST_ERROR);
    }
    return true;
  }
}

export class DeleteUserRequest {
  username;
  constructor({ username }) {
    this.username = username;
  }

  validate() {
    if (!this.username) {
      throw new Error(DELETE_USER_REQUEST_ERROR);
    }
    return true;
  }
}
