import express from "express";
import userRouter from "./user.js";

const router = express.Router();

router.get("/", (req, res) => {
  return res.send("api is running...");
});

router.use("/users", userRouter);

export default router;
