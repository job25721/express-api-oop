import express from "express";
import controllers from "../controllers/index.js";

const router = express.Router();

router.get("/get-list", controllers.userController.getUsers);
router.post("/register", controllers.userController.register);
router.delete("/:username", controllers.userController.deleteUser);

export default router;
