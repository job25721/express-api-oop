import { Sequelize } from "sequelize";
import dotenv from "dotenv";
dotenv.config();

const config = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
};

let sequelize = new Sequelize({ ...config, dialect: "mysql" });

const createClient = () => {
  sequelize = new Sequelize({ ...config, dialect: "mysql" });
};

const connect = async () => {
  if (sequelize === null) {
    createClient();
  }

  try {
    await sequelize.authenticate();
    console.info(
      "Database connection established successfully @" +
        config.host +
        ":" +
        config.port
    );
  } catch (error) {
    console.error("connection error: ", error);
    throw error;
  }
};

const disconnect = () => sequelize.close();

const getClient = async () => {
  if (sequelize === null) {
    await connect();
  }
  return sequelize;
};

export default { connect, disconnect, getClient };

// export class DataStore {
//   static config = {
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     username: process.env.DB_USER,
//     password: process.env.DB_PASSWORD,
//     database: process.env.DB_NAME,
//   };
//   static instance = null;

//   constructor({ host, port, username, password, database }) {
//     this.sequelize = new Sequelize({
//       host,
//       port,
//       dialect: "mysql",
//       username,
//       password,
//       database,
//     });
//   }

//   sequelize;

//   static getInstance() {
//     if (this.instance === null) {
//       this.instance = new DataStore(this.config);
//     }
//     return this.instance;
//   }

//   static async connect() {
//     return this.sequelize.authenticate();
//   }

//   async disconnect() {
//     return this.sequelize.close();
//   }
// }
