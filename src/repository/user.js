import { QueryTypes } from "sequelize";
import database from "../connections/sequelize.js";

const save = async ({ username, password }) => {
  const sequelize = await database.getClient();
  return sequelize.query(
    `insert into user(username, password) values("${username}","${password}")`
  );
};

const findUsers = async () => {
  const sequelize = await database.getClient();
  return sequelize.query("select * from user", {
    type: QueryTypes.SELECT,
  });
};

const deleteUserByUsername = async ({ username }) => {
  const sequelize = await database.getClient();
  const result = await sequelize.query(
    `delete from user where username = '${username}'`
  );
  return result[0].affectedRows;
};

export default {
  findUsers,
  save,
  deleteUserByUsername,
};
