import express from "express";
import cors from 'cors'
import sequelize from "./src/connections/sequelize.js";
import appRouter from "./src/router/index.js";

const app = express();
app.use(cors())
app.use(express.json());
app.use(appRouter);

const port = process.env.PORT || 8080;

sequelize
  .connect()
  .then(() => {
    app.listen(port, () => {
      console.log("server listening on port " + port);
    });
  })
  .catch((err) => {});
